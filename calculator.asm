SECTION .data
    msg1 db 'Enter the first number', 0Ah, 0h
    msg2 db 'What operation do you want to do?', 0Ah, 0h
    msg3 db 'Enter 1, 2, 3, or 4', 0Ah, 0h
    msg4 db '1. Add', 0Ah, 0h
    msg5 db '2. Subtract', 0Ah, 0h
    msg6 db '3. Multiply', 0Ah, 0h
    msg7 db '4. Divide', 0Ah, 0h
    msg8 db 'Enter the second number', 0Ah, 0h
    msg9 db 'Answer = ', 0h
    msg10 db ' Remainder = ', 0h
    ; All the text messages I want to display

SECTION .bss
    sinput: RESB 255
    firstint: RESB 4
    secondint: RESB 4
    operator: RESB 4
    result: RESB 4
    remainder: RESB 4
    ; The different undefined variables I will use

SECTION .text
global _start

_start:
    ; Get the user to input the first number
    mov eax, msg1 ; Copy the address of msg1 to the register
    call writestr ; Go to the subroutine writestr
    call strinput ; Same as above with subroutine strinput
    mov eax, sinput ; Move the address of sinput to the register
    call strtoint
    mov [firstint], eax ;Move the value of the register into the firstint variable
    ; Get the user to choose what calculation to do
    call operation
    ; Get the user to input the second number
    mov eax, msg8 
    call writestr
    call strinput
    mov eax, sinput
    call strtoint
    mov [secondint], eax
    ; Perform the calculation
    call calculation
    ; Print the answer
    mov eax, msg9
    call writestr
    mov eax, [result] ; Move the value of the variable into the register
    call writeint
    ; If dividing, it has a slightly different answer format so split if it is dividing
    mov eax, [operator]
    cmp eax, 4 ; Compare the value in the register and the value 4
    je .remainderanswer ; If the above is equal, continue instructions at the label .remainderanswer
    call writeLF
    call quit

.remainderanswer:
    mov eax, msg10
    call writestr
    mov eax, [remainder]
    call writeint
    call writeLF
    call quit

operation:
    push eax ; Take the value in the register and store it on top of the stack
    push ebx
    push ecx
    push edx

    ; List all the instructions and options
    mov eax, msg2
    call writestr
    mov eax, msg3
    call writestr
    mov eax, msg4
    call writestr
    mov eax, msg5
    call writestr
    mov eax, msg6
    call writestr
    mov eax, msg7
    call writestr
    call strinput
    mov eax, sinput
    call strtoint
    ; Store the result from strtoint and store as the variable operator
    mov [operator], eax

    pop edx ; Take the value on top of the stack and put in the register
    pop ecx
    pop ebx
    pop eax
    ret ; Return to where the subroutine was called from

calculation:
    push eax
    push ebx
    push ecx
    push edx

    ; Get the 3 parts that are needed ready
    mov eax, [firstint]
    mov ebx, [secondint]
    mov edx, [operator]
    ; Jump if the operator value is equal to one of the options chosen, otherwise quit
    cmp edx, 1
    je .add
    cmp edx, 2
    je .subtract
    cmp edx, 3
    je .multiply
    cmp edx, 4
    je .divide
    call quit

.add:
    add eax, ebx
    mov [result], eax
    jmp .finish
.subtract:
    sub eax, ebx
    mov [result], eax
    jmp .finish
.multiply:
    mul ebx ; Multiply the value in eax by ebx
    mov [result], eax
    jmp .finish
.divide:
    xor edx, edx ; Clear the value in edx
    div ebx ; Divide eax by edx, then store the result in eax and remainder in edx
    mov [result], eax
    mov [remainder], edx
    jmp .finish

.finish:
    ; Restore to how it was before starting and return to where it was called
    pop edx
    pop ecx
    pop ebx
    pop eax
    ret

strinput:
    push edx
    push ecx
    push ebx
    push eax
    mov edx, 255
    mov ecx, sinput ; Destination for the user input
    mov ebx, 0
    mov eax, 3 ; System call condition for reading the user input
    int 80h ; Interrupt based on the conditions set above
    pop eax
    pop ebx
    pop ecx
    pop edx
    ret

strtoint:
    push ebx
    push ecx
    push edx
    push esi
    mov esi, eax
    mov eax, 0
    mov ecx, 0

.mulltiplyLoop:
    ; Take the ASCII character, if it is not between 48(0) and 57(9) go to the finish
    xor ebx, ebx
    mov bl, [esi+ecx]
    cmp bl, 48
    jl .finished
    cmp bl, 57
    jg .finished
    ; Convert to the actual value by removing 48, add it to the result and multiply by 10
    sub bl, 48
    add eax, ebx
    mov ebx, 10
    mul ebx
    inc ecx ; increase the offset by 1 byte
    jmp .mulltiplyLoop ; Go back to the beginning of this section


.finished:
    ; As long as the offset is not 0 (It actually is a number), divide by 10
    cmp ecx, 0
    je .restore
    mov ebx, 10
    div ebx

.restore:
    ; Go back to where this all was requested with the result stored in eax
    pop esi
    pop edx
    pop ecx
    pop ebx
    ret

writeint:
    push eax
    push ecx
    push edx
    push esi
    mov ecx, 0

.divideLoop:
    ; Get the number of digits by dividing by 10, comparing the result to 0
    ; Take the remainder, convert to ASCII (add 48) and store on the stack
    inc ecx ; Increase by 1
    mov edx, 0
    mov esi, 10
    idiv esi
    add edx, 48
    push edx
    cmp eax, 0
    jnz .divideLoop

.printLoop:
    ; Take each character on the stack and print it
    ; Use the number of digits from above to loop the correct number of times
    dec ecx ; Decrease by 1
    mov eax, esp
    call writeChar
    pop eax
    cmp ecx, 0
    jnz .printLoop

    pop esi
    pop edx
    pop ecx
    pop eax
    ret

writestr:
    ; Print the string (need the length from strlen to be stored in edx)
    mov ecx, eax
    call strlen
    mov edx, eax
    mov ebx, 1
    mov eax, 4 ; System call condition for writing
    int 80h
    ret

writeLF:
    ; Print a return line (move to new line)
    push eax
    mov eax, 0Ah ; 0A is the hex code for a new line ("h" at the end for hex)
    push eax
    mov eax, esp
    call writestr
    pop eax
    pop eax
    ret

strlen:
    push ebx
    mov ebx, eax

.nextchar:
    ; If the character is NULL(00000000), go to finished, otherwise loop and increase eax to the next byte
    cmp byte [eax], 0
    jz .strlenfinished
    inc eax
    jmp .nextchar

.strlenfinished:
    sub eax, ebx
    pop ebx
    ret

writeChar:
    ; Print 1 character from eax
    push edx
    push ecx
    push ebx
    mov ecx, eax
    mov edx, 1
    mov eax, 4
    mov ebx, 1
    int 80h
    pop ebx
    pop ecx
    pop edx
    ret

quit:
    ; Close the program gracefully
    mov ebx, 0
    mov eax, 1
    int 80h
    ret
